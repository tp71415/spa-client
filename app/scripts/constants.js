// notice that we are adding these variables into
// global namespace, so please add 'BT_' prefix to variable name

// colors
var
  BT_COLOR_BLUE = '#029fe4',
  BT_COLOR_ORANGE = '#fcbd00';
'use strict';

/**
 * @ngdoc directive
 * @name boincClientApp.directive:Loader
 * @description
 * # Loader
 */
angular.module('boincClientApp')
  .directive('btLoader', function () {
    return {
      template: '<div class="bt-loader">' +
      '<div class="bounce1"></div> ' +
      '<div class="bounce2"></div> ' +
      '<div class="bounce3"></div></div>',
      restrict: 'EA',
      replace: true,
      scope: {
        show: '='
      }
    };
  });


/**
 * @ngdoc directive
 * @name boincClientApp.directive:Loader
 * @description
 * # Loader
 */
angular.module('boincClientApp')
  .directive('btLoaderContainer', function () {
    return {
      template: '<div class="bt-loader-container">' +
      '<div ng-if="show" bt-loader></div>' +
      '<div ng-hide="show" class="bt-loader-content">' +
      '<ng-transclude></ng-transclude>' +
      '</div></div>',
      restrict: 'EA',
      transclude: true,
      scope: {
        show: '='
      }
    };
  });


/**
 * @ngdoc directive
 * @name boincClientApp.directive:ButtonLoader
 * @description
 * Button loader
 * Will show a loader instead of a button content.
 *
 * Usage: as a attribute in <button> or <a> or any element.
 *
 * @param show {boolean}   if is set to true, a loader is shown.
 */
angular.module('boincClientApp')
  .directive('btButtonLoader', function () {
    return {
      restrict: 'A',
      template: '<bt-loader ng-show="loading"></bt-loader>' +
      '<span ng-hide="loading"><ng-transclude></ng-transclude></span>',
      scope: {
        loading: '=btButtonLoader'
      },
      transclude: true,
      link: function(scope, el, attr) {
        // manually add class to element
        el.addClass('bt-button-loader');
      }
    };
  });


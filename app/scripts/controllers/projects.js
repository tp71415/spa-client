'use strict';

/**
 * @ngdoc function
 * @name boincClientApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the boincClientApp
 */
angular.module('boincClientApp')
  .controller('ProjectsCtrl', ['$scope', 'AppService', '$timeout',
    function ($scope, AppService, $timeout) {

      $scope.apps = [];
      $scope.view = {
        loading: true
      };

      AppService.all()
        .then(function(apps) {
          $scope.apps = apps;
          $scope.view.loading = false;
        });

      $scope.donutData = [
        {
          value: 60,
          color: BT_COLOR_BLUE,
          label: 'Done'
        },
        {
          value: 20,
          color: BT_COLOR_ORANGE,
          label: 'Done by you'
        },
        {
          value: 20,
          color: '#fff',
          label: 'Remaining'
        }
      ];

      // Chart.js Options
      $scope.options = {
        animation: false,
        ///Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines : true,

        //String - Colour of the grid lines
        scaleGridLineColor : 'rgba(0,0,0,.05)',

        //Number - Width of the grid lines
        scaleGridLineWidth : 1,

        //Boolean - Whether the line is curved between points
        bezierCurve : true,

        //Number - Tension of the bezier curve between points
        bezierCurveTension : 0.4,

        //Boolean - Whether to show a dot for each point
        pointDot : true,

        //Number - Radius of each point dot in pixels
        pointDotRadius : 4,

        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth : 1,

        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,

        //Boolean - Whether to show a stroke for datasets
        datasetStroke : true,

        //Number - Pixel width of dataset stroke
        datasetStrokeWidth : 2,

        //Boolean - Whether to fill the dataset with a colour
        datasetFill : true,

        //String - A legend template
        legendTemplate : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'

      };

    }]);

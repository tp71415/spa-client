'use strict';

/**
 * @ngdoc function
 * @name boincClientApp.controller:ResetpasswordCtrl
 * @description
 * # ResetpasswordCtrl
 * Controller of the boincClientApp
 */
angular.module('boincClientApp')
  .controller('ResetpasswordCtrl', ['$scope', 'AuthService', '$state', '$stateParams',
    function ($scope, AuthService, $state, $stateParams) {

      $scope.resetData = {};
      $scope.send = false;
      $scope.loading = false;

      $scope.sendResetLink = function () {
        $scope.loading = true;

        AuthService.sendResetLink($scope.resetData.email)
          .then(function () {
            $scope.send = true;
            //$state.go('reset-password');
          }, function () {
            //
          })
          .finally(function () {
            $scope.loading = false;
          });
      };

      $scope.resetPassword = function () {
        $scope.loading = true;

        AuthService.resetPassword(
          $scope.resetData.password,
          $scope.resetData.passwordConfirm,
          $stateParams.token)
          .then(function () {
            $scope.send = true;
          }, function () {
            //
          })
          .finally(function () {
            $scope.loading = false;
          });

      };

    }
  ]);

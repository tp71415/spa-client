'use strict';

/**
 * @ngdoc function
 * @name boincClientApp.controller:AppCtrl
 * @description
 * # AppCtrl
 * Controller of the boincClientApp
 */
angular.module('boincClientApp')
  .controller('AppCtrl', ['$scope', 'AuthService', '$state', '$filter', '$modal',
    function ($scope, AuthService, $state, $filter, $modal) {

      var isModalOpened = false;

      $scope.$on('errors.http', function() {
        if (! isModalOpened) {
          isModalOpened = true;

          var modalInstance = $modal.open({
            templateUrl: 'modalError',
            controller: function ($scope, $modalInstance) {
              $scope.close = function () {
                $modalInstance.close();
              };
            },
            size: 'sm',
            windowClass: 'modal-error'
          });

          modalInstance.result.then(function (selectedItem) {
            isModalOpened = false;
          }, function() {
            isModalOpened = false;
          });
        }
      });

      $scope.$state = $state;

      $scope.loggedUser = {
        'username': ''
      };

      $scope.loggedUser = AuthService.getUserInfo();

      $scope.logout = function () {
        AuthService.logout().then(function() {
          $state.go('login');
        });
      };

    }]);

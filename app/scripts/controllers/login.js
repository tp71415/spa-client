'use strict';

/**
 * @ngdoc function
 * @name boincClientApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the boincClientApp
 */
angular.module('boincClientApp')
  .controller('LoginCtrl', ['$scope', 'AuthService', '$state',
    function ($scope, AuthService, $state) {

      $scope.error = '';
      $scope.loading = false;

      $scope.login = function () {
        $scope.loading = true;

        AuthService.login($scope.loginData.username, $scope.loginData.password)
          .then(function () {
            $state.go('app.projects');
          }, function () {
            $scope.error = 'Invalid username/password';
          })
          .finally(function () {
            $scope.loading = false;
          });
      };

      $scope.signUp = function () {
        $scope.loading = true;

        AuthService.signUp($scope.signUpData.username, $scope.signUpData.password, $scope.signUpData.passwordConfirm)
          .then(function () {
            $state.go('login');
          }, function () {
            $scope.error = 'Passwords do not match or are too short (at least 8 characters)';
          })
          .finally(function () {
            $scope.loading = false;
          });
      };

    }]);

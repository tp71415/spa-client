'use strict';

/**
 * @ngdoc function
 * @name boincClientApp.controller:ProjectCtrl
 * @description
 * # ProjectCtrl
 * Controller of the boincClientApp
 */
angular.module('boincClientApp')
  .controller('ProjectCtrl', function ($scope) {

    // Chart.js Data
    $scope.data = {
      labels: ['11 Nov', '12 Nov', '13 Nov', '14 Nov', '15 Nov', 'Yesterday', 'Today'],
      datasets: [
        {
          label: 'How much I compute',
          fillColor: 'rgba(1,160,226,0.2)',
          strokeColor: BT_COLOR_BLUE,
          pointColor: BT_COLOR_BLUE,
          pointStrokeColor: '#fff',
          pointHighlightFill: '#fff',
          pointHighlightStroke: 'rgba(151,187,205,1)',
          data: [0.5, 3, 2, 4, 1.5, 4, 5]
        }
      ]
    };

    // Chart.js Options
    $scope.options = {

      ///Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines : true,

      //String - Colour of the grid lines
      scaleGridLineColor : 'rgba(0,0,0,.05)',

      //Number - Width of the grid lines
      scaleGridLineWidth : 1,

      //Boolean - Whether the line is curved between points
      bezierCurve : true,

      //Number - Tension of the bezier curve between points
      bezierCurveTension : 0.4,

      //Boolean - Whether to show a dot for each point
      pointDot : true,

      //Number - Radius of each point dot in pixels
      pointDotRadius : 4,

      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth : 1,

      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,

      //Boolean - Whether to show a stroke for datasets
      datasetStroke : true,

      //Number - Pixel width of dataset stroke
      datasetStrokeWidth : 2,

      //Boolean - Whether to fill the dataset with a colour
      datasetFill : true,

      //String - A legend template
      legendTemplate : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'

    };

  });

'use strict';

/**
 * @ngdoc function
 * @name boincClientApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the boincClientApp
 */
angular.module('boincClientApp')
  .controller('SettingsCtrl', ['$scope', 'SettingsService',
    function ($scope, SettingsService) {

      $scope.view = {
        loading: true
      };

      SettingsService.all().then(function() {
        $scope.view.loading = false;
      });

    }
  ]);

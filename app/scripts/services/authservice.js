'use strict';

/**
 * @ngdoc service
 * @name boincClientApp.AuthService
 * @description
 * # AuthService
 * Service in the boincClientApp.
 */
angular.module('boincClientApp')
  .service('AuthService', ['$q', '$window', '$http', '$timeout',
    function ($q, $window, $http, $timeout) {

      var userInfo = null;    // data about logged user

      /**
       * Loads user info from local storage
       */
      function init() {
        if ($window.localStorage.userInfo) {
          userInfo = JSON.parse($window.localStorage.userInfo || {});
        }
      }

      /**
       * Returns user info
       *
       * @returns {object}
       */
      function getUserInfo() {
        return userInfo;
      }

      /**
       * Logout current user
       *
       * @returns {boolean}
       */
      function logout() {
        var deferred = $q.defer();

        $http.delete(BT_HOST + '/users/sign_out.json')
          .success(function (d) {
            $window.localStorage.userInfo = null;
            userInfo = null;
            console.log('[DEBUG]');
            console.log(d);
            deferred.resolve();
          })
          .error(function (d) {
            console.log('[DEBUG]');
            console.log(d);
            deferred.reject();
          });

        return deferred.promise;
      }

      /*
       * Login user with credentials
       *
       * @param string username
       * @param string password
       * @returns promise
       */
      function login(username, password) {
        var deferred = $q.defer();

        $http({
          url: BT_HOST + '/users/sign_in.json',
          method: 'POST',
          data: {
            'user': {
              email_addr: username,
              password: password
            }
          }
        })
          .success(function (r) {
            console.log('[DEBUG]');
            console.log(r);

            userInfo = {
              token: 'test',
              username: username
            };
            $window.localStorage.userInfo = JSON.stringify(userInfo);

            deferred.resolve();
          })
          .error(function (r) {
            console.log('[debug]');
            console.log(r);
            deferred.reject(r);
          });


        return deferred.promise;
      }

      /**
       * Sign up a new user
       *
       * @param {string} username
       * @param {string} password
       * @param {string} password_confirm
       *
       * @returns {object} promise
       */
      function signUp(username, password, passwordConfirm) {
        var deferred = $q.defer();

        $http({
          url: BT_HOST + '/users.json',
          method: 'POST',
          data: {
            user: {
              email_addr: username,
              password: password,
              password_confirmation: passwordConfirm
            }
          }
        }).success(function (r) {
          deferred.resolve();
        }).error(function (r) {
          deferred.reject('');
        });

        return deferred.promise;
      }


      function changePassword() {
        //
      }

      /**
       * Sends a password reset email to user.
       *
       * @param email
       * @returns {*}
       */
      function sendResetLink(email) {
        var deferred = $q.defer();

        $timeout(function() {
          deferred.resolve(email);
        },1000);

        return deferred.promise;
      }

      /**
       * Change user's password via reset (token must be provided)
       *
       * @param email
       * @param emailConfirm
       * @param token
       * @returns {*}
       */
      function resetPassword(email, emailConfirm, token) {
        var deferred = $q.defer();

        $timeout(function() {
          deferred.resolve();
        }, 1000);

        return deferred.promise;
      }

      // initialize the service
      init();

      return {
        login: login,
        signUp: signUp,
        getUserInfo: getUserInfo,
        logout: logout,
        changePassword: changePassword,
        sendResetLink: sendResetLink,
        resetPassword: resetPassword
      };

    }]);

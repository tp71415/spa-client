
'use strict';

/**
 * @ngdoc service
 * @name boincClientApp.Project
 * @description
 * # Project
 * Service in the boincClientApp.
 */
angular.module('boincClientApp')
  .service('AppService', ['$http', '$q', '$timeout',
    function ($http, $q, $timeout) {

      var
        service = {},
        projects = [],
        lastSync = 0;

      /**
       * Fetch all projects.
       * Response can be resolved promise with projects,
       * or rejected with error.
       *
       * @returns {*} promise
       */
      service.all = function () {
        var deferred = $q.defer();

        $http.get('scripts/services/projects.json')
          .success(function (data) {
            projects = data;
            $timeout(function() {
              deferred.resolve(projects);
            },1000);
          })
          .error(function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      };


      /**
       * Fetch a project with given id.
       *
       * @param id string
       * @returns {*} promise
       */
      service.get = function (id) {
        var deferred = $q.defer();

        $http.get('scripts/services/project.json')
          .success(function (data) {
            deferred.resolve(data);
          })
          .error(function (error) {
            deferred.reject(error);
          });

        return deferred.promise;
      };

      return service;
    }
  ]);

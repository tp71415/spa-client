'use strict';

/**
 * @ngdoc service
 * @name boincClientApp.SettingsService
 * @description
 * # SettingsService
 * Service in the boincClientApp.
 */
angular.module('boincClientApp')
  .service('SettingsService', ['$http', '$timeout', '$q',
    function ($http, $timeout, $q) {

      var service = {};

      service.all = function () {
        var deferred = $q.defer();

        $timeout(function() {
          deferred.resolve();
        },1000);

        return deferred.promise;
      };

      return service;

    }
  ]);

'use strict';

// if config is not present, set default variables
// in development environment
if (! BT_HOST) {
  var BT_HOST = 'http://localhost:3000';
}

/**
 * @ngdoc overview
 * @name boincClientApp
 * @description
 * # boincClientApp
 *
 * Main module of the application.
 */
angular
  .module('boincClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'tc.chartjs'
  ])

  .run(function ($rootScope, $state) {
    $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
      if (error && error.authenticated === false) {
        return $state.go('login');
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: '/views/app.html',
        controller: 'AppCtrl',
        resolve: {
          authenticated: ['$q', 'AuthService', function ($q, AuthService) {
            var userInfo = AuthService.getUserInfo();
            if (userInfo && userInfo.token) {
              return $q.when(userInfo);
            } else {
              return $q.reject({authenticated: false});
            }
          }]
        }
      })

      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })

      .state('signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'LoginCtrl'
      })

      .state('forgot-password', {
        url: '/forgot-password',
        templateUrl: 'views/reset-password-form.html',
        controller: 'ResetpasswordCtrl'
      })

      .state('reset-password', {
        url: '/reset-password/:token',
        templateUrl: 'views/reset-password.html',
        controller: 'ResetpasswordCtrl'
      })

      .state('app.projects', {
        url: '/projects',
        views: {
          mainView: {
            templateUrl: 'views/projects/projects.html',
            controller: 'ProjectsCtrl'
          }
        }
      })

      .state('app.projectdetail', {
        url: '/projects/:id',
        views: {
          mainView: {
            templateUrl: 'views/projects/project.html',
            controller: 'ProjectCtrl'
          }
        }
      })

      .state('app.settings', {
        url: '/settings',
        abstract: false,
        views: {
          mainView: {
            templateUrl: 'views/settings/settings.html',
            controller: 'SettingsCtrl'
          }
        }
      })

      .state('app.settings.processor', {
        url: '/processor',
        views: {
          settingsView: {
            templateUrl: 'views/settings/processor.html'
          }
        }
      })

      .state('app.settings.disk', {
        url: '/disk',
        views: {
          settingsView: {
            templateUrl: 'views/settings/disk.html'
          }
        }
      })

      .state('app.settings.network', {
        url: '/network',
        views: {
          settingsView: {
            templateUrl: 'views/settings/network.html'
          }
        }
      })

      .state('app.profile', {
        url: '/profile',
        abstract: true,
        views: {
          mainView: {
            templateUrl: 'views/profile/profile.html'
          }
        }
      })

      .state('app.messages', {
        url: '/messages',
        views: {
          mainView: {
            templateUrl: 'views/profile/messages/messages.html'
          }
        }
      })

      .state('app.messages.inbox', {
        url: '/inbox',
        views: {
          messageView: {
            templateUrl: 'views/profile/messages/inbox.html'
          }
        }
      })

      .state('app.messages.outbox', {
        url: '/outbox',
        views: {
          messageView: {
            templateUrl: 'views/profile/messages/outbox.html'
          }
        }
      })

      .state('app.messages.new', {
        url: '/new',
        views: {
          messageView: {
            templateUrl: 'views/profile/messages/new.html'
          }
        }
      })

      .state('app.profile.account', {
        url: '/account',
        views: {
          profileView: {
            templateUrl: 'views/profile/account-settings.html'
          }
        }
      })

      .state('app.profile.passwordChange', {
        url: '/password-change',
        views: {
          profileView: {
            templateUrl: 'views/profile/password-change.html'
          }
        }
      });

    // if none of the above states are matched, use this as a fallback
    $urlRouterProvider.otherwise('/app/projects');

    $httpProvider.interceptors.push(function($q, $rootScope) {
      return {
        'responseError': function(response) {
          $rootScope.$broadcast('errors.http');
          return $q.reject(response);
        }
      };
    });

  });




'use strict';

describe('Controller: ProjectsCtrl', function () {

  // load the controller's module
  beforeEach(module('boincClientApp'));

  var ctrl,
      scope;

  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ctrl = $controller('ProjectsCtrl', {
      $scope: scope
    });
  }));

  it('it should init', function () {
    expect(scope.test.length).toBe(3);
  });
});

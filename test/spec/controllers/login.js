'use strict';

describe('Controller: LoginCtrl', function () {

  // load the controller's module
  beforeEach(module('boincClientApp'));

  var $httpBackend,
    authRequestHandler,
    $rootScope,
    createController,
    ctrl,
    scope;

  beforeEach(inject(function ($injector, $controller, $rootScope) {
    $httpBackend = $injector.get('$httpBackend');

    //authRequestHandler = $httpBackend.when('POST', 'http://localhost:3000/users/sign_in.json')
    //  .respond({success: 'success'});

    // Get hold of a scope (i.e. the root scope)
    $rootScope = $injector.get('$rootScope');
    // The $controller service is used to create instances of controllers
    var $controller = $injector.get('$controller');

    createController = function() {
      return $controller('LoginCtrl', {'$scope' : $rootScope });
    };
  }));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  //it('should fetch authentication token', function() {
  //  var controller = createController();
  //  $httpBackend.flush();
  //
  //  $httpBackend.expectPOST('http://localhost:3000/users/sign_in.json')
  //    .respond({success: 'success'});
  //
  //  $rootScope.login('test@test.sk','testtest');
  //  $httpBackend.flush();
  //});

  //it('error should be none on init', function () {
  //  expect(scope.error).toBe('');
  //});

});

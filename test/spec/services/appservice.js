'use strict';

describe('Service: AppService', function () {

  // load the service's module
  beforeEach(module('boincClientApp'));

  // instantiate service
  var App;
  beforeEach(inject(function (_AppService_) {
    App = _AppService_;
  }));

  it('should do something', function () {
    expect(!!App).toBe(true);
  });

});

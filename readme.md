# Vývoj a údržba klientskej časti aplikácie projektu BOINC

Tento dokument obsahuje postupy, ktoré sú povinní dodržiavať všetky človenovia tímu pri upravovaní a pridávaní funkcionality do klientskej časti BOINC aplikácie (ďalej len aplikácie). Tento dokument nerozoberá spôsob správneho písania kódu, je odporúčané používať “best practises” frameworkov, ktoré používame.

##Stručne o aplikácií

Aplikácia je vyvíjaná pomocou frameworku [AngularJS](https://angularjs.org/). Dokumentáciu k frameworku môžete nájst [tu](https://docs.angularjs.org/). SPA je napojená na REST API, ktoré poskytuje serverová aplikácia v repozitári rest-backend. Preto je pri vývoji potrebné mať túto serverovú aplikácie lokálne spustenú, viac informácií v readme súbore spomenutého repozitára.

### Pojmy, používané v tomto dokumente
SPA - Single Page Application
REST - Representational state transfer

### Štruktúra aplikácie

V projekte musí byť dodržaná nasledujúca adresárová štruktúra

.
+-- _app - obsahuje všetky zdrojové kódy aplikácie
|   +-- images - statické obrázky
|   +-- scripts - súbory javascriptu
|   +-- controllers - controllery frameworku Angular
|   +-- services - služby framework Angular
|   +-- styles - css súbory
|   +-- views - angular html šablóny
+-- _bower_components* - obsahuje externé knižnice
+-- _dist* - obsahuje zbuildovanú aplikáciu
+-- _node_modules* - obsahuje nodejs moduly
+-- _test - obsahuje testovacie súbory

Priečinky označené hviezdičkou* sa nenachádzajú v repozitári projektu, každý vývojár si ich vytvára lokálne, pomocou nástrojov opísaných nižšie.

## Inicializácia a nastavenie projektu

Na vyvíjanie projektu je potebné mať nainštalovaný [NodeJS](http://nodejs.org/) a jeho package manager NPM. V prípade, že nemáte nainštalovaný NodeJS a NPM, najjednoduchšie je si stiahnuť [inštalačný balíček](http://nodejs.org/download/) a postupovať podľa pokynov.

Medzi NodeJS moduly, ktoré je potrebné mať nainštalované (globálne), patrí

- [Grunt](http://gruntjs.com/) - nástroj na automatizáciu úloh
- [Bower](http://bower.io/) - správa front-end pluginov tretích strán
- [Yeoman](http://yeoman.io/) - nástroj na automatizované vytváranie nových súborov, špecifiických pre použitý framework

Viac informácií o jednotlivých nástrojoch nájdete na vyššie uvedených stránkach projektov.

Tieto závislosti nainštalujete týmto príkazom (podľa nastavenia adresára NPM možno bude potrebné použiť sudo)
```
npm install -g grunt-cli bower yo
```
Ďalej je potrebné nainštalovať yeoman generátor s názvom generator-angular
```
npm install --global generator-angular
```
Po naklonovaní repozitáru je potrebné lokálne nainštalovať závislosti projektu. Spustite príkazy.
```
npm install
bower install
```
Po vykonaní predchádzajúcich krokov je aplikácia pripravená na spustenie. Na spustenie lokálneho servera, zadajte príkaz
```
grunt serve
```
Po vykonaní tohto príkazu by sa malo v prehliadači automaticky otvoriť okno s bežiacou aplikáciou.

Upozornenie: nikdy nespúšťajte aplikáciu priamo z app/index.html, ale vždy spustite lokálny server.


##Príprava pred vývojom

Po stiahnutí najnovšej verzie zo servera je vždy potrebné spustiť príkazy.
```
npm update
bower update
```
Tieto príkazy nainštalujú, alebo aktualizujú, moduly, ktoré do projektu pridal iný člen tímu, a sú potrebné na beh aplikácie.

Pri tvorbe kódu a následnej kontrole funkcionality v prehliadači, je užitočná nasledujúce úloha: (spustite v inom tabe alebo okne terminálu, ako samostatný proces)
```
grunt watch
```
Táto úloha spôsobí pri každej zmene zdrojového kódu, kompiláciu a automatický refresh otvorenej aplikácie, s aktualizovanýmy zdrojovými kódmi.
Veľmi dôležitá je úloha `js:hint`, ktorá spustí kontrolu javascript kódu. Každý pull-request s nenulovým počtom chýb bude automaticky zamietnutý.

Pridávanie modulov tretej strany

Kvôli ľahšiemu managementu modulov tretej strany a ich verzíí (modul na vykresľovanie grafov, frameworky) je nutné, pokiaľ je to možné, ich pridávať ako bower moduly. Viac informácií o tom, ako používať bower, nájdete tu (http://bower.io/). Informáciu o názve bower modulu je väčšinou možné nájsť na stránkach projektu daného modulu, alebo pomocou príkazu
```
bower search <nazov_modulu>
```
Na inštaláciu modulu do projektu zadajte (nezabudnite byť nastavený v priečinku projektu)
```
bower install <nazov_modulu> --save
```
Upozornenie: je dôležité použiť prepínač `--save`, v opačnom prípade by sa modul neuložil do zoznamu modulov v bower.json, a ostaným členom tímu by sa tento modul nenainštaloval.

Aby sa súbory modulu (vo väčšine prípadov súbory javascriptu a štýlov) automaticky nalinkovali do projektu (súbor app/index.html), spustite
```
grunt wiredep
```
Odteraz môžete používať funkcie nainštalovaného modulu.

##Pridávanie funkcionality do aplikácie

Aplikácia je napísaná vo frameworku AngularJS. Vytváranie controllerov, služieb a views je zautomatizované pomocou nástroje Yeoman.
```
yo angular:controller myController
yo angular:directive myDirective
yo angular:filter myFilter
yo angular:service myService
```

Tieto príkazy automatický generujú spec súbory potrebné na unit testy. Tie sa nachádzajú v zložke test/spec. Unit testy spustíte príkazom
```
grunt test
```
Ku každému vytvorenému controlleru/služby/filtru je potrebné napísať unit test. Každá použitá premenná a metóda, ktorú je obsiahnutá v `scope`, musí mať napísaný aspoň jeden test pri inicializácií, a jeden pri zmene.

##Build aplikácie

Build aplikácie nie je nutné vykonávať, vykonáva sa len pri deployovaní na server.
```
grunt build
```

V prípade akýchkoľvek nejasností pri postupoch vyvíjania, prosím kontaktujte @PatrikGallik prostredníctvom HipChatu.
